#include <iostream>
#include <Poco/Util/ServerApplication.h>
#include <Poco/Util/AbstractConfiguration.h>
#include <glog/logging.h>

class TaskApp: public Poco::Util::ServerApplication
{
public:
    
protected:
    int main(const std::vector<std::string>& args)
    {
        LOG(INFO) << config().getString("application.name");
        return 0;
    }
};

POCO_SERVER_MAIN(TaskApp)